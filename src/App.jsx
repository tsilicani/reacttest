import React from "react";
import styled from "styled-components";

import AppleSignin from "react-apple-signin-auth";

const MyAppleSigninButton = () => (
  <AppleSignin
    authOptions={{
      clientId: "io.jungler.app",
      redirectURI: "https://tsilicani.gitlab.io/reacttest/",
      state: "state",
      nonce: "nonce",
      usePopup: true,
    }}
    onSuccess={(response) => console.log(response)}
    onError={(error) => console.error(error)}
    render={(props) => <Button {...props}> Apple</Button>}
  />
);

const Layout = styled.div`
  width: 100vw;
  height: 100vh;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const Button = styled.button`
  background: white;
  border-radius: 24px;
  width: 240px;
  height: 48px;
  border: 2px solid #8e669a;
  cursor: pointer;
`;

function App() {
  return (
    <div className="App">
      <Layout>
        <MyAppleSigninButton />
      </Layout>
    </div>
  );
}

export default App;
